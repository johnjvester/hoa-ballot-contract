# `hoa-ballot-contract` Repository

> The `hoa-ballot-contract` repository is focused on establishing a web3 smart contract using [Infura](https://infura.io/),
> [The Truffle Framework](https://www.trufflesuite.com/), [Solidity](https://solidity.readthedocs.io/en/v0.7.1/),
> [hdwallet](https://www.npmjs.com/package/@truffle/hdwallet-provider) and [Ganache](https://www.trufflesuite.com/ganache).

## Publications

This repository is related to a DZone.com publication:

* [Diving Deep into Smart Contracts](https://dzone.com/articles/diving-deep-into-smart-contracts)

To read more of my publications, please review one of the following URLs:

* https://dzone.com/users/1224939/johnjvester.html
* https://johnjvester.gitlab.io/dZoneStatistics/WebContent/#/stats?id=1224939


## Using This Repository

At a high level, the following steps are required to utilize this repository:

1. `npm install -g truffle`
2. `mkdir hoa-ballot-contract && cd hoa-ballot-contract`
3. `truffle init`
4. `npm install @openzeppelin/contracts`
5. `truffle dashboard`
6. `truffle migrate --network dashboard`
7. Note the `contract address` for the `2_hoaballot_migration.js` transaction
8. Validate the contract was deployed using the https://ropsten.etherscan.io/address/PUT_YOUR_CONTRACT_ADDRESS_HERE URL


## Next Steps

To see how a [React](https://reactjs.org/) client can be created to interact with this smart contact, please review the following repository:

[hoa-ballot-client](https://gitlab.com/johnjvester/hoa-ballot-client)


## Additional Information

Made with <span style="color:red;">♥</span> &nbsp;by johnjvester@gmail.com, because I enjoy writing code.
